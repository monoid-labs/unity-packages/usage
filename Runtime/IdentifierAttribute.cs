﻿using System.Text.RegularExpressions;
using UnityEngine;

namespace Monoid.Unity.Assets {

  public static class Identifiers {

    public static bool Validate(string id) => regex.IsMatch(id);

    static readonly Regex regex = new Regex(IDENTIFIER, RegexOptions.Compiled);

    // http://msdn.microsoft.com/en-us/library/aa664670(v=vs.71).aspx
    const string FORMATTING_CHARACTER = @"\p{Cf}";
    const string CONNECTING_CHARACTER = @"\p{Pc}";
    const string DECIMAL_DIGIT_CHARACTER = @"\p{Nd}";
    const string COMBINING_CHARACTER = @"\p{Mn}|\p{Mc}";
    const string LETTER_CHARACTER = @"\p{Lu}|\p{Ll}|\p{Lt}|\p{Lm}|\p{Lo}|\p{Nl}";
    const string IDENTIFIER_PART_CHARACTER = LETTER_CHARACTER + "|" +
                                             DECIMAL_DIGIT_CHARACTER + "|" +
                                             CONNECTING_CHARACTER + "|" +
                                             COMBINING_CHARACTER + "|" +
                                             FORMATTING_CHARACTER;
    const string IDENTIFIER_PART_CHARACTERS = "(" + IDENTIFIER_PART_CHARACTER + ")+";
    const string IDENTIFIER_START_CHARACTER = "(" + LETTER_CHARACTER + "|_)";
    const string IDENTIFIER = "^" + IDENTIFIER_START_CHARACTER + "(" + IDENTIFIER_PART_CHARACTERS + ")*$";
  }


  public sealed class IdentifierAttribute : PropertyAttribute { }

}
