﻿using UnityEngine;
using UnityEditor;

namespace Monoid.Unity.Assets {

  public abstract class DelayedEditor : Editor, ApplyRevertGUI.IEditor {

    readonly ApplyRevertGUI gui;

    public DelayedEditor() {
      gui = new ApplyRevertGUI(this);
    }

    protected virtual void OnApply() { }
    protected virtual void OnRevert() { }

    /// returns error message or an empty string if valid
    protected virtual string Validate() => string.Empty;


    #region ApplyRevertGUI.IEditor

    public bool IsDirty() => serializedObject.hasModifiedProperties;

    public void Apply() {
      serializedObject.ApplyModifiedProperties();
      OnApply();
    }

    public void Revert() {
      serializedObject.Update();//UpdateIfRequiredOrScript();
      OnRevert();
    }

    #endregion

    #region Unity Messages

    // https://github.com/Unity-Technologies/UnityCsReference/blob/master/Modules/AssetPipelineEditor/ImportSettings/AssetImporterEditor.cs

    protected virtual void OnEnable() {
      EditorApplication.wantsToQuit += ShowModificationsPopupQuit;
      serializedObject.Update();
    }

    protected virtual void OnDisable() {
      EditorApplication.wantsToQuit -= ShowModificationsPopupQuit;
      if (IsDirty()) {
        ShowModificationsPopupDisable();
      }
    }

    #endregion

    public override void OnInspectorGUI() {
      if (!(target is ScriptableObject)) {
        // MonoBehaviours are not supported, fallback to default inspector GUI
        base.OnInspectorGUI();
        return;
      }

      DrawProperties();

      string err = Validate();
      bool valid = err.Length == 0;

      gui.Draw(valid);
      if (valid) {
        return;
      }

      EditorGUILayout.HelpBox(err, MessageType.Error);
    }

    protected virtual void DrawProperties() => DrawPropertiesExcluding(serializedObject);


    void ShowModificationsPopupDisable() => gui.ShowModificationsPopup(Validate().Length == 0);
    bool ShowModificationsPopupQuit() => IsDirty() ? gui.ShowCancelableModificationsPopup(Validate().Length == 0) : true;

  }

}
