﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Monoid.Unity.Assets {

  public class Usage : EditorWindow {

    static readonly string[] DefaultSearchFolders = { "Assets" };

    public enum AssetType {
      Material, Shader, Script
    }

    public static Type Type(AssetType type) {
      switch (type) {
        case AssetType.Material:
          return typeof(Material);
        case AssetType.Shader:
          return typeof(Shader);
        case AssetType.Script:
          return typeof(MonoScript);
        default:
          return null;
      }
    }

    [SerializeField] AssetType type;
    [SerializeField] UnityEngine.Object filter;
    [SerializeField] UnityEngine.Object[] assets;
    Vector2 scrollPosition;

    [MenuItem("Window/Assets/Find Usage", priority = 3010)]
    static void Init() {
      var window = EditorWindow.GetWindow<Usage>();
      window.titleContent.text = "Asset Usage";
      window.Show();
    }

    public void OnGUI() {
      EditorGUI.BeginChangeCheck();
      type = (AssetType)EditorGUILayout.EnumPopup("Type", type);
      if (EditorGUI.EndChangeCheck()) {
        filter = null;
        assets = null;
      }

      Type filterType = Type(type);
      if (filterType != null) {
        EditorGUI.BeginChangeCheck();
        filter = EditorGUILayout.ObjectField($"{type}", filter, filterType, false);
        if (EditorGUI.EndChangeCheck()) {
          assets = null;
        }
      }

      if (GUILayout.Button("Find Usage")) {
        switch (type) {
          case AssetType.Material:
            assets = FindPrefabs(filter ? (Material)filter : null);
            break;
          case AssetType.Shader:
            assets = FindMaterials(filter ? (Shader)filter : null);
            break;
          case AssetType.Script:
            assets = FindObjects(filter ? (MonoScript)filter : null);
            break;
          default:
            assets = new UnityEngine.Object[0];
            break;
        }
        if (assets != null) {
          LogAssets.Info(assets);
        }
      }

      if (assets == null) {
        return;
      }

      scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
      var obj = new SerializedObject(this);
      SerializedProperty prop = obj.FindProperty("assets");
      EditorGUILayout.PropertyField(prop, true);
      EditorGUILayout.EndScrollView();

      GUI.enabled = assets.Length > 0;
      if (GUILayout.Button("Select")) {
        Selection.objects = assets;
      }
      GUI.enabled = true;

    }

    #region Material

    public static GameObject[] FindPrefabs(Material material) => FindPrefabs(material, DefaultSearchFolders);

    public static GameObject[] FindPrefabs(Material material, string[] searchFolders) {
      var prefabs = new List<GameObject>();
      string[] guids = AssetDatabase.FindAssets("t:Model t:Prefab", searchFolders);
      foreach (string guid in guids) {
        string path = AssetDatabase.GUIDToAssetPath(guid);
        GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
        if (!prefab) {
          continue;
        }
        if (Has(material, prefab)) {
          prefabs.Add(prefab);
        }
      }
      return prefabs.ToArray();
    }

    static bool Has(Material material, GameObject prefab) {
      foreach (Renderer r in prefab.GetComponentsInChildren<Renderer>(true)) {
        foreach (Material mat in r.sharedMaterials) {
          if (mat == material) {
            return true;
          }
        }
      }
      return false;
    }

    #endregion

    #region Shader

    public static Material[] FindMaterials(Shader shader) => FindMaterials(shader, DefaultSearchFolders);

    public static Material[] FindMaterials(Shader shader, string[] searchFolders) {
      var materials = new List<Material>();
      string[] guids = AssetDatabase.FindAssets("t:Material", searchFolders);
      foreach (string guid in guids) {
        string path = AssetDatabase.GUIDToAssetPath(guid);
        Material material = AssetDatabase.LoadAssetAtPath<Material>(path);
        if (!material) {
          continue;
        }
        if (shader ? material.shader == shader : material.shader.name == "Hidden/InternalErrorShader") {
          materials.Add(material);
        }
      }
      return materials.ToArray();
    }

    #endregion

    #region Script

    public static UnityEngine.Object[] FindObjects(MonoScript script) => FindObjects(script, DefaultSearchFolders);

    public static UnityEngine.Object[] FindObjects(MonoScript script, string[] searchFolders) {
      var type = script?.GetClass();
      if (type == null || typeof(MonoBehaviour).IsAssignableFrom(type)) {
        return FindPrefabs(type, searchFolders);
      }
      if (typeof(ScriptableObject).IsAssignableFrom(type)) {
        return FindScriptableObjects(type, searchFolders);
      }
      return null;
    }
    public static GameObject[] FindPrefabs(Type type, string[] searchFolders) {
      var prefabs = new List<GameObject>();
      string[] guids = AssetDatabase.FindAssets("t:Prefab", searchFolders);
      foreach (string guid in guids) {
        string path = AssetDatabase.GUIDToAssetPath(guid);
        GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
        if (!prefab) {
          continue;
        }
        if (Has(type, prefab)) {
          prefabs.Add(prefab);
        }
      }
      return prefabs.ToArray();
    }
    public static ScriptableObject[] FindScriptableObjects(Type type, string[] searchFolders) {
      string typeName = type.FullName;
      var objects = new List<ScriptableObject>();
      string[] guids = AssetDatabase.FindAssets($"t:{typeName}", searchFolders);
      foreach (string guid in guids) {
        string path = AssetDatabase.GUIDToAssetPath(guid);
        ScriptableObject so = AssetDatabase.LoadAssetAtPath<ScriptableObject>(path);
        if (!so) {
          continue;
        }
        objects.Add(so);
      }
      return objects.ToArray();
    }

    static bool Has(Type type, GameObject prefab) => type != null ? prefab.GetComponentInChildren(type, true) : AnyNull(prefab.GetComponentsInChildren<MonoBehaviour>(true));
    // static bool Has(Type type, GameObject prefab) => prefab.GetComponentInChildren(type, true);
    static bool Has(MonoScript script, GameObject prefab) => Has(script?.GetClass(), prefab);

    static bool AnyNull(UnityEngine.Object[] objects) {
      foreach (var obj in objects) {
        if (!obj) {
          return true;
        }
      }
      return false;
    }

    #endregion

  }

}
