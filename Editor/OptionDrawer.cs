﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEditor;

namespace Monoid.Unity.Assets {

  [CustomPropertyDrawer(typeof(OptionAttribute))]
  public sealed class OptionDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      if (property.propertyType != SerializedPropertyType.String) {
        goto Default;
      }

      OptionAttribute attr = attribute as OptionAttribute;
      if (attr == null) {
        goto Default;
      }

      string name = property.stringValue;
      string[] names = attr.labels;

      int index = 0; // null or "" default to first value
      bool valid = name?.Length > 0;
      if (valid) {
        index = Array.IndexOf(names, property.stringValue); ;
      }

      int missing = (index < 0) ? 1 : 0;

      var display = new GUIContent[names.Length + missing];
      for (int i = 0; i < names.Length; i++) {
        display[i] = new GUIContent(names[i]);
      }

      if (missing > 0) {
        index = names.Length;
        display[index] = new GUIContent(name + " (unknown)");
      }

      // necessary due to a unity bug
      label.tooltip = fieldInfo.GetCustomAttribute<TooltipAttribute>()?.tooltip ?? string.Empty;

      EditorGUI.BeginProperty(position, label, property);
      EditorGUI.BeginChangeCheck();
      index = EditorGUI.Popup(position, label, index, display);
      if (EditorGUI.EndChangeCheck() || !valid) {
        if (index < names.Length) {
          property.stringValue = names[index].ToString();
        }
      }
      EditorGUI.EndProperty();
      return;

    Default:
      EditorGUI.PropertyField(position, property, label, true);
    }
  }

}
