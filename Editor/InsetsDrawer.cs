using UnityEngine;
using UnityEditor;

namespace Monoid.Unity.Assets {

  [CustomPropertyDrawer(typeof(InsetsAttribute))]
  public sealed class InsetsDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      // NOTE(micha): to lazy to propertly test / support multi-editing
      if (property.serializedObject.isEditingMultipleObjects) {
        EditorGUI.PropertyField(position, property, label, true);
        return;
      }

      InsetsAttribute insets = (InsetsAttribute)attribute;

      SerializedProperty inField = property.FindPropertyRelative("from");
      SerializedProperty outField = property.FindPropertyRelative("to");
      // vector support
      if (inField == null) {
        inField = property.FindPropertyRelative("x");
      }
      if (outField == null) {
        outField = property.FindPropertyRelative("y");
      }

      float inValue = inField.floatValue;
      float outValue = outField.floatValue;

      float PADDING = 4 * EditorGUIUtility.pixelsPerPoint;
      float FIELD = EditorGUIUtility.fieldWidth;

      EditorGUI.BeginChangeCheck();
      label = EditorGUI.BeginProperty(position, label, property);
      position = EditorGUI.PrefixLabel(position, label);

      int indent = EditorGUI.indentLevel;
      EditorGUI.indentLevel = 0;
      { // from
        Rect fromPos = position;
        fromPos.width = FIELD;
        inValue = EditorGUI.FloatField(fromPos, inValue);
        inValue = Mathf.Max(0, inValue);
        if (inValue + outValue > insets.range) {
          inValue = insets.range - outValue;
        }
      }
      { // slider
        Rect sliderPos = position;
        sliderPos.x += FIELD + PADDING;
        sliderPos.width -= 2 * FIELD + 2 * PADDING;

        float min = inValue / insets.range;
        float max = 1 - (outValue / insets.range);
        EditorGUI.MinMaxSlider(sliderPos, ref min, ref max, 0, 1);
        inValue = min * insets.range;
        outValue = (1 - max) * insets.range;
      }
      { // to
        Rect toPos = position;
        toPos = position;
        toPos.x += toPos.width - FIELD;
        toPos.width = FIELD;
        outValue = EditorGUI.FloatField(toPos, outValue);
        outValue = Mathf.Max(0, outValue);
        if (inValue + outValue > insets.range) {
          outValue = insets.range - inValue;
        }
      }
      EditorGUI.indentLevel = indent;
      EditorGUI.EndProperty();

      bool changed = EditorGUI.EndChangeCheck();
      changed |= inValue != inField.floatValue;
      changed |= outValue != outField.floatValue;

      if (!changed) {
        return;
      }

      inField.floatValue = inValue;
      outField.floatValue = outValue;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
      return property.serializedObject.isEditingMultipleObjects
           ? EditorGUI.GetPropertyHeight(property, true)
           : EditorGUIUtility.singleLineHeight;
    }
  }
}
