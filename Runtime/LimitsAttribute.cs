using UnityEngine;

namespace Monoid.Unity.Assets {

  public sealed class LimitsAttribute : PropertyAttribute {
    public readonly float from, to;

    public LimitsAttribute() : this(0, 1) { }

    public LimitsAttribute(float from, float to) {
      this.from = from;
      this.to = to;
    }
  }

}
