using UnityEditor;
using UnityEngine;

namespace Monoid.Unity.Assets {

  public static class AssetReserialization {

    const int MenuPriority = 222;

    /// Folder to process. Default is the root `Assets` folder. Local package paths need to be set menually if desired.
    public static string[] Folders = { "Assets" };

    [MenuItem("Assets/Reserialize/All", priority = MenuPriority)]
    public static void All() => All(false);

    public static void All(bool fast) => All(fast, Folders);

    public static void All(bool fast, params string[] folders) {
      string[] assetPaths = AssetDatabase.FindAssets(string.Empty, folders);
      for (int n = assetPaths.Length, i = 0; i < n; i++) {
        assetPaths[i] = AssetDatabase.GUIDToAssetPath(assetPaths[i]);
      }

      if (fast) {
        AssetDatabase.ForceReserializeAssets(assetPaths);//, ForceReserializeAssetsOptions.ReserializeAssetsAndMetadata);
        AssetDatabase.Refresh();
        return;
      }

      Run(assetPaths);
    }

    [MenuItem("Assets/Reserialize/Selected", priority = MenuPriority)]
    public static void Selected() => Selected(false);

    public static void Selected(bool fast) {
      Object[] selected = Selection.objects;

      string[] assetPaths = new string[selected.Length];
      for (int n = assetPaths.Length, i = 0; i < n; i++) {
        assetPaths[i] = AssetDatabase.GetAssetPath(selected[i]);
      }

      if (fast) {
        AssetDatabase.ForceReserializeAssets(assetPaths);//, ForceReserializeAssetsOptions.ReserializeAssetsAndMetadata);
        AssetDatabase.Refresh();
        return;
      }

      Run(assetPaths);
    }

    public static void Run(params string[] assetPaths) {
      try {
        string[] single = { string.Empty };
        for (int n = assetPaths.Length, i = 0; i < n; i++) {
          string assetPath = assetPaths[i];
          if (EditorUtility.DisplayCancelableProgressBar("Reserialize Assets", assetPath, i / (float)n)) {
            return;
          }
          if (assetPath.Length > 0) {
            single[0] = assetPath;
            AssetDatabase.ForceReserializeAssets(single);//, ForceReserializeAssetsOptions.ReserializeAssetsAndMetadata);
          }
        }
      } finally {
        EditorUtility.ClearProgressBar();
        AssetDatabase.Refresh();
      }
    }

  }

}
