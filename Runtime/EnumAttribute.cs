using System;
using UnityEngine;

namespace Monoid.Unity.Assets {

  public sealed class EnumAttribute : PropertyAttribute {
    public readonly Type type;

    public EnumAttribute(Type type) {
      if (!type.IsEnum) {
        throw new ArgumentException($"Type needs to ne an enum: {type.FullName}");
      }
      this.type = type;
    }
  }

}
